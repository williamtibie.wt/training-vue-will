﻿export interface IEmployeeModel {
    id?: number;
    name?: string;
    email?: string;
    password?: string;
}