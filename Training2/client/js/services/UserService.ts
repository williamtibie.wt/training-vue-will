﻿import Axios from 'axios';
import { IEmployeeModel } from '../models/IEmployeeModel';

export class UserService {
    employees: IEmployeeModel[] = [];
    employee: IEmployeeModel = {};

    async insertUser(value: IEmployeeModel) {
        try {
            let response = await Axios.post<boolean>('/api/v1/user/insert', value);

            console.log(response.data);
        } catch (error) {
            console.log(error.response.data);
        }
    }

    async getUsers() {
        let response = await Axios.get<IEmployeeModel[]>('/api/v1/user')
        this.employees = response.data;
    }

    async getSpecificUser(id: number) {
        let response = await Axios.get<IEmployeeModel>('/api/v1/user/' + id);
        this.employee = response.data;
    }
}

export let userServiceSingleton = new UserService();