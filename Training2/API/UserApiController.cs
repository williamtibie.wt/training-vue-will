﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Training2.Services;

namespace Training2.API
{
    [Route("api/v1/user")]
    [ApiController]
    public class UserApiController : ControllerBase
    {
        private readonly UserService ServiceMan;

        public UserApiController(UserService service)
        {
            this.ServiceMan = service;
        }

        [HttpPost("insert")]
        public async Task<IActionResult> AddNewEmployeeAsync([FromBody]EmployeeModel value)
        {
            if(ModelState.IsValid == false)
            {
                return BadRequest("Model is not valid");
            }

            var isSuccess = await ServiceMan.InsertNewEmployeeAsync(value);

            if(isSuccess == false)
            {
                return BadRequest("Failed to insert db");
            }
            return Ok("Success to insert db");
        }

        [HttpGet]
        public async Task<IActionResult> GetAllEmployee()
        {
            var employees = await ServiceMan.GetAllEmployee();
            if(employees.Count == 0)
            {
                return BadRequest();
            }
            return Ok(employees);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetAllEmployee(int id)
        {
            var employees = await ServiceMan.GetEmployee(id);
            if (employees == null)
            {
                return BadRequest();
            }
            return Ok(employees);
        }
    }
}