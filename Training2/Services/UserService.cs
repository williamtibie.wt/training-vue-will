﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Training2.Entities;

namespace Training2.Services
{
    public class EmployeeModel
    {
        public int Id { set; get; }
        public string Name { set; get; }
        public string Email { set; get; }
        public string Password { set; get; }
    }

    public class UserService
    {
        private readonly TrainingDbContext _Db;

        public UserService(TrainingDbContext db)
        {
            this._Db = db;
        }

        public async Task<bool> InsertNewEmployeeAsync(EmployeeModel value)
        {
            var newEmployee = new Employee
            {
                EmployeeName = value.Name,
                EmployeeEmail = value.Email,
                EmployeePassword = value.Password
            };

            _Db.Employee.Add(newEmployee);
            await _Db.SaveChangesAsync();
            return true;
        }

        public async Task<EmployeeModel> GetEmployee(int id)
        {
            var employee = await _Db.Employee.Where(Q => Q.EmployeeId == id).Select(Q=>new EmployeeModel
            {
                Email = Q.EmployeeEmail,
                Id = Q.EmployeeId,
                Name = Q.EmployeeName,
                Password = Q.EmployeePassword
            }).FirstOrDefaultAsync();
            return employee;
        }

        public async Task<List<EmployeeModel>> GetAllEmployee()
        {

            var employee = await _Db.Employee.AsNoTracking().Select(Q => new EmployeeModel
            {
                Email = Q.EmployeeEmail,
                Id = Q.EmployeeId,
                Name = Q.EmployeeName,
                Password = Q.EmployeePassword
            }).ToListAsync();
            
            return employee;
        }
    }
}
